FROM osrf/ros:humble-desktop

#EXPOSE 1024 # needed for VS Code. Must be 1024 or higher

# Set up robot user and create the home directory, etc
ARG USERNAME=robot
RUN adduser --disabled-password --gecos "" $USERNAME && \
    chown -R $USERNAME /home/$USERNAME && \
    adduser $USERNAME sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER $USERNAME
WORKDIR "/home/$USERNAME"

RUN sudo apt-get update && \
    sudo apt-get install -y git && \
    sudo apt-get install -y python3-pip && \
    sudo apt-get install -y ros-humble-gazebo-* && \
    sudo apt install -y ros-humble-cartographer && \
    sudo apt install -y ros-humble-cartographer-ros && \ 
    sudo apt install -y ros-humble-navigation2 && \
    sudo apt install -y ros-humble-nav2-bringup && \
    sudo apt install -y ros-humble-dynamixel-sdk && \
    sudo apt install -y ros-humble-turtlebot3-* && \
    sudo apt install -y ros-humble-tf-transformations && \
    sudo pip3 install transforms3d && \
    sudo apt install -y x11-apps

RUN echo 'export ROS_DOMAIN_ID=30' >> ~/.bashrc && \
    export ROS_DOMAIN_ID=30 && \
    echo 'export TURTLEBOT3_MODEL=burger' >> ~/.bashrc && \
    export TURTLEBOT3_MODEL=burger && \
    echo 'export DONT_PROMPT_WSL_INSTALL=1' >> ~/.bashrc && \
    export DONT_PROMPT_WSL_INSTALL=1 && \
    echo 'source /opt/ros/humble/setup.bash' >> ~/.bashrc

WORKDIR "/home/$USERNAME/"
RUN mkdir -p "/home/$USERNAME/turtlebot3_ws/src"
WORKDIR "/home/$USERNAME/turtlebot3_ws/src"
RUN git clone https://git.exeter.ac.uk/drv201/csmm418_teleop.git
RUN mv csmm418_teleop/dv_teleop/ dv_teleop 
RUN rm -rf csmm418_teleop/

WORKDIR "/home/$USERNAME/"

RUN cd "/home/$USERNAME/turtlebot3_ws/" && \
    colcon build --symlink-install  && \
    rosdep update  && \
    rosdep install --from-path src --rosdistro humble 

# You need to run this in after installing,
# in "/home/$USERNAME/turtlebot3_ws/ :
# . install/setup.bash





