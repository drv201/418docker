# ROS 2 simulation system in Docker
The dockerfile in this folder setups up a docker container with:
- all the necessary ROS2 libraries to get Gazebo running
- support for the Turtlebot3
- an example application called `dv_teleop`

The dockerfile contains a Linux box running Ubuntu 22.04 (Jammy), and
ROS2 Humble. The dockerfile enables a user called `robot`, who has `sudo` 
rights - you can use `sudo` with this user if you need to.

Inside `robot`'s home director: `/home/robot`, a ROS2 development folder
is created with the name: `turtlebot3_ws`. To get things going, after creating the image and starting a container, open two terminals. In both, go to `turtlebot3_ws`.

In one run:

    ros2 run dv_teleop listener

and in the other one run:

    ros2 launch turtlebot3_gazebo empty_world.launch.py

It is possible that nothing may happen. In particular, launching turtlebot3 gazebo for the first time takes a long time. If nothing happens, try stopping one or both applications (using `CTRL-C`) and restarting. If all goes well, in Gazebo, you'll see a robot going around in fairly tight circles.

# Building
You should delete this readme before building, otherwise it doesn't play nice with `docker build`

## docker build:
Inside this directory, run:

    docker build -t <image_name> .

then

    docker run --name <container_name> -it -e DISPLAY=host.docker.internal:0.0 <image_name>

This should work and drop you into a Ros terminal window in Docker. To open more terminals into the same container, just go

    docker exec -it <container_name> bash
